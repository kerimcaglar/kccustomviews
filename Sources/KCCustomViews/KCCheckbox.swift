//
//  KCCheckbox.swift
//  
//
//  Created by Kerim Caglar on 3.10.2021.
//

import UIKit

@IBDesignable
public class KCCheckbox: UIControl {
    
    private weak var checkboxImageView: UIImageView!
      
      private var checkboxImage: UIImage {
        return checked ? UIImage(named: "check")! : UIImage(named: "uncheck")!
      }
      
      @IBInspectable
      open var checked: Bool = false {
        didSet {
          checkboxImageView.image = checkboxImage
        }
      }
      
      override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
      }
      
      required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
      }
      
      private func commonInit() {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        
        imageView.image = checkboxImage
        imageView.contentMode = .scaleAspectFit
        
        self.checkboxImageView = imageView
        
        backgroundColor = UIColor.clear
        
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
      }
      
      @objc func touchUpInside() {
        checked = !checked
        sendActions(for: .valueChanged)
      }
}

