import XCTest
@testable import KCCustomViews

final class KCCustomViewsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(KCCustomViews().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
